package com.rocketplan;

import javax.faces.webapp.FacesServlet;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RocketplanApplication.class);
	}
	
	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
	    FacesServlet servlet = new FacesServlet();
	    ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(servlet, "*.xhtml");
	    return servletRegistrationBean;
	}
	
}